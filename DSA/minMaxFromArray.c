#include<stdio.h>

void getMin(int *arr,int term) {

    int min = 99999;
    int iterative;
    for(iterative = 0;iterative < term;iterative++) {

        if(arr[iterative] < min) 
            min = arr[iterative];
    }
    printf("\nmin : %d",min);
}
void getMax(int *arr, int term) {

    int max = -1;
    int iterative;

    for(iterative = 0;iterative < term;iterative++) {

        if(max < arr[iterative])
            max = arr[iterative]; 
    }
    printf("\nmax : %d\n",max);
}
int main() {

    
    int max = -1;
    int term,iteration;

    int arr[10];

    printf("Enter the number of terms  : \t");
    scanf("%d",&term);

    printf("Enter the numbers : \t");
    for(iteration = 0;iteration < term;iteration++) {

        scanf("%d",&arr[iteration]);
    }
    getMin(arr,term);
    getMax(arr,term);
}