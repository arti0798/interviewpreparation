// Write a Program  to sort a string in alphabetical order. Given a string, the task is to sort the string in alphabetical order and display it as output.
// ❗ Language restrictions use C language

// For example, consider the following strings.

// Input: focus

// Output: cfosu

#include<stdio.h>
#include<string.h>

int main() {

    char str[10],str2[10],temp;

    printf("Enter the string : \t");
    scanf("%s",str);
    int i,j;
    int l = strlen(str);

    for(i = 0;i < l-1;i++) {

        printf("%c ********:   %c \n",str[i],str[i+1]);
        for(j = i+1;j < l;j++) {
            if(str[i] > str[j]) {

                printf("%c :   %c \n",str[i],str[i+1]);
                temp = str[i];
                str[i] = str[j];
                str[j] = temp;
            }
        }
    }
    printf("%s\n",str);
}