#include<stdio.h>
#include<stdlib.h>

struct node {
   
   int info;
   struct node *next;
};

struct node * getNode(int value) {

    struct node *temp = NULL;
    temp = (struct node *)malloc(sizeof(struct node *));
    temp->info = value;
    temp->next = NULL;
    return temp;
}
void initList(struct node **h) {

    *h = getNode(0);
}
void insert(struct node *h,int value) {

    struct node *temp = getNode(value);
    struct node * p = h;
    // struct node *q = h->next;
    
    if(p->next == NULL) {

        p->next = temp;
        return;
    }

    while(p->next != NULL) {

        p = p->next;
    }
    p->next = temp;

    printf("*************  %d\n",p->info);
    h->info++;
    return;
}
void display(struct node *h) {

    struct node *p = h->next;

    // printf("in display\n");
    while(p != NULL) {

        printf("%d ----- ",p->info);
        p = p->next;
    }
}
void freeNode(struct node *q) {

    free(q);
}
void delete(struct node *h, int pos) {

    struct node * p = h;
    // struct node *temp = p->next;
    struct node *q = p->next;

    if(pos > h->info) {

        printf("Invalid position");
        exit(1);
    } 
    int i = 0;
    while(i < pos-1) {

        p = q;
        q = q->next;
        i++;
    }
    p->next = q->next;
    q->next = NULL;
    freeNode(q);
    h->info--;
    printf("Deleted successfully!!\n");
    


}
void search(struct node *h, int element) {

    struct node *p = h->next;

    while(p) {

        if(p->info == element) {

            printf("\nElement found\n");
            exit(0);
        }
    }
    printf("Element not found!!\n");
    exit(1);
}
void reverseLinkList(struct node **head){

    struct node* previous = NULL;
    struct node *current = (*head)->next;
    struct  node *nextNode;

    // printf("\nI m in reverse\n");
    while(current!=NULL) {

        // printf("\nI m in reverse loop\n");
        nextNode = current->next;
        current->next = previous;

        previous = current;
        current = nextNode;

    }
    *head = previous;
}
int main() {

    struct node *head = NULL;
    int n,value;

    initList(&head);
    // printf("Enter choice : \t");
    // scanf("%d",&n);

    while(1) {

        printf("Enter choice : \t");
        printf("\n1 : Insert  2: Display  3:Delete  4:Search 5:Reverse linkList");
        scanf("%d",&n);

        switch(n) {

            case 1: printf("Enter value : \t");
                    scanf("%d",&value);
                    insert(head,value);
                    printf("Successful\n");
                    break; 

            case 2: display(head);
                    break;   

            case 3: printf("Enter the position of the element which you want to: \t");
                    scanf("%d",&value);
                    delete(head, value);   
                    break;  

            case 4: printf("Enter the element for search : \t");
                    scanf("%d",&value);
                    search(head,value); 
                    break;

            case 5: printf("Reverse linklist :\t");
                    reverseLinkList(&head);
                    display(head);
                    break;                       
        }
    }

}