package Java;

import java.util.HashSet;
import java.util.Set;

public class hashSet {
    
    public static void main(String[] args) {
        
        Set<String> hashSet = new HashSet<>();

        hashSet.add("AA");
        hashSet.add("BB");
        hashSet.add("C");
        hashSet.add("C");

        System.out.println(hashSet);

        System.out.println(hashSet.contains("C"));
        System.out.println(hashSet.contains("CC"));
    }
}
