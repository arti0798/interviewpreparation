package leetCodeSol.java;

import java.util.ArrayList;
import java.util.List;

public class arrayList {
  
    public static void main(String[] args) {
        
        List<Integer> arrList = new ArrayList<>(5);
        
        for(int i = 0;i < 5;i++) {

            arrList.add(i);
        }

        System.out.println(arrList);

        arrList.remove(2);

        System.out.println(arrList);

        for(int i = 0;i<arrList.size();i++) {

            System.out.println(arrList.get(i) + "  ");
        }

        for(int i : arrList) {

            System.out.println(i);
        }
    }
}
