package leetCodeSol.java;
import java.util.*;

class returnArrayPasToGetTarget {

    public static int[] twoSum(int []nums,int target) {

        Map<Integer, Integer> map = new HashMap<>();

        for(int i = 0;i < nums.length;i++) {

            map.put(nums[i], i);
        }
        for(int i = 0;i < nums.length;i++) {

            System.out.println("***************************************************");
            System.out.println("\nmap..containskey :  " + map.containsKey(target - nums[i]));
            System.out.println("\nmap.get(target-nums[i]) :    "+ map.get(target-nums[i]));
            
            if(map.containsKey(target -nums[i]) && i != map.get(target-nums[i]))
                return new int[] {i, map.get(target - nums[i])};
        }

        return new int[2];
    }

    // public static int[] twoSum(int[] nums, int target) {
       
    //     try{
    //         for(int i = 0;i < nums.length;i++) {

    //             for(int j = i+1; j < nums.length;i++) {

    //                 if(nums[i]+nums[j] == target) {

    //                     return new int[]{i,j};
    //                 }
    //             }
    //         }
           
    //     }
    //     catch(Exception e) {
            
    //         System.out.println(e);
    //     }
    //     return new int[2];
    // }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.println("How many number you want to enter : \t ");
        n = sc.nextInt();
        
        
        int []nums = new int[n];

        System.out.println("Enter element : ");
        for(int i = 0;i < n;i++) {

            nums[i] = sc.nextInt();
        }
        for(int i = 0;i < n;i++) {

            System.out.println("  " + nums[i]);
        }
        System.out.println("Enter the target : \n");
        int target = sc.nextInt();
        nums = twoSum(nums,target);

        for(int i = 0;i < 2;i++)
            System.out.println("nums  : "+nums[i]);
        sc.close();
    }
}