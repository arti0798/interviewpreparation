package leetCodeSol.java;
import java.util.*;

public class hashMapDemo {
 
    public static void main(String[] args) {
        
        HashMap<String, String> obj1 = new HashMap<>();

        obj1.put("1", "arti");
        obj1.put("2", "artiSingh");
        obj1.put("5", "azad");
        obj1.put("3", "khushi");
        obj1.put("3", "khushiSingh");

        System.out.println(obj1);
        
        String v = obj1.get("3");
        
        System.out.println("value : "+v);
    }
}
