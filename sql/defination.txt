sudo -u postgres psql

primary key:  primary key constraint uniquely identifies each record in a table.
                primary key must contain unique value and cannot be null.
                A table can have only one primary key.
                A primary key constraint automatically has a UNIQUE constaraint.


unique Key:  A unique constraint ensures that all values in a cloumn are different.

             We can have many UNIQUE constraint per table but only one primary key constraint per table.

Difference between primary key and unique key: **
**********************************************
    primary key
    -------------
    It is used to add integrity constraints to the table. 
    Only one primary key is allowed to be used in a table.
    Duplicate and NULL (empty) values are not valid in the case of the primary key. 
    Primary keys can be used as foreign keys for other tables too.    

    unique key
    ----------
    Unique key is a constraint that is used to uniquely identify a tuple in a table.



Foreign key
------------
    foreign key used to link two table together.
    foreign key is a field in one table that refers to the primary key in other table.
    The table containing the foreign key is called the child table, and the table containing 
        the candidate key is called the referenced or parent table.



Primary key AND Candidate key
-----------------------------
Primary Key is a set of attributes (or attribute) which uniquely identify the tuples in relation or table.
 The primary key is a minimal super key, so there is one and only one primary key in any relationship.

candidate key
------------
A candidate key is a set of attributes (or attribute) which uniquely identify the tuples in relation or table.
As we know that Primary key is a minimal super key, so there is one and only one primary key in any relationship 
but there is more than one candidate key can take place.
Candidate key’s attributes can contain a NULL value which opposes to the primary key. 


** Its confirmed that a primary key is a candidate key.
** But Its confirmed that a candidate key can be a primary key.



Super Key:
-----------
Super Key is an attribute (or set of attributes) that is used to uniquely identifies all attributes in a relation. 
All super keys can’t be candidate keys but its reverse is true. In a relation, number of super keys are more than number of candidate keys.

Candidate Key is a proper subset of a super key.


Composite key
--------------
A primary key having two or more attributes is called composite key. It is a combination of two or more columns.



JOINS
------
A JOIN clause is used to combine rows from two or more tables, based on a related column between them.

\c testing
 create table customer(custNo int primary key,cname varchar(20),address varchar(20));
create table orders(oNo int primary key,oname varchar(20),date date,custNo int,foreign key(custNo) references customer(custNo) on delete cascade on update cascade);
insert into customer values(1,'Arti','Pune');
--------------------------------------------------------------------------------------
*** (INNER) JOIN: Returns records that have matching values in both tables

select * from orders o INNER JOIN customer ON o.custno = customer.custno;
 ono |  oname  |    date    | custno | custno | cname  | address  
-----+---------+------------+--------+--------+--------+----------
  11 | qqq     | 2001-01-01 |      1 |      1 | Arti   | Pune
  12 | www     | 2002-02-02 |      1 |      1 | Arti   | Pune
  13 | aaa     | 2002-02-02 |      2 |      2 | Heena  | Pune
  14 | fff     | 2002-03-02 |      3 |      3 | Khushi | Pune
  15 | ggg     | 2002-03-03 |      4 |      4 | Pooja  | Jiyanpur
  16 | ggghh   | 2005-05-03 |      5 |      5 | Azad   | Jiyanpur
  17 | ggkkghh | 2005-05-03 |      6 |      6 | Anand  | varansi
(7 rows)

--------------------------------------------------------------------------------
*** LEFT (OUTER) JOIN: Returns all records from the left table, and the matched records from the right table

select * from orders o LEFT JOIN customer ON o.custno = customer.custno;
 ono |  oname  |    date    | custno | custno | cname  | address  
-----+---------+------------+--------+--------+--------+----------
  11 | qqq     | 2001-01-01 |      1 |      1 | Arti   | Pune
  12 | www     | 2002-02-02 |      1 |      1 | Arti   | Pune
  13 | aaa     | 2002-02-02 |      2 |      2 | Heena  | Pune
  14 | fff     | 2002-03-02 |      3 |      3 | Khushi | Pune
  15 | ggg     | 2002-03-03 |      4 |      4 | Pooja  | Jiyanpur
  16 | ggghh   | 2005-05-03 |      5 |      5 | Azad   | Jiyanpur
  17 | ggkkghh | 2005-05-03 |      6 |      6 | Anand  | varansi
  10 | ccc     | 2999-09-09 |        |        |        | 
(8 rows)
-------------------------------------------------------------------------------------------------------------

*** RIGHT (OUTER) JOIN: Returns all records from the right table, and the matched records from the left table

select * from orders o RIGHT JOIN customer ON o.custno = customer.custno;
 ono |  oname  |    date    | custno | custno |    cname    | address  
-----+---------+------------+--------+--------+-------------+----------
  11 | qqq     | 2001-01-01 |      1 |      1 | Arti        | Pune
  12 | www     | 2002-02-02 |      1 |      1 | Arti        | Pune
  13 | aaa     | 2002-02-02 |      2 |      2 | Heena       | Pune
  14 | fff     | 2002-03-02 |      3 |      3 | Khushi      | Pune
  15 | ggg     | 2002-03-03 |      4 |      4 | Pooja       | Jiyanpur
  16 | ggghh   | 2005-05-03 |      5 |      5 | Azad        | Jiyanpur
  17 | ggkkghh | 2005-05-03 |      6 |      6 | Anand       | varansi
     |         |            |        |      8 | Virat singh | Jiyanpur
     |         |            |        |      7 | Azad singh  | Jiyanpur
(9 rows)
-------------------------------------------------------------------------------------------------------------
*** FULL (OUTER) JOIN: Returns all records when there is a match in either left or right table

select * from orders o FULL JOIN customer ON o.custno = customer.custno;
 ono |  oname  |    date    | custno | custno |    cname    | address  
-----+---------+------------+--------+--------+-------------+----------
  11 | qqq     | 2001-01-01 |      1 |      1 | Arti        | Pune
  12 | www     | 2002-02-02 |      1 |      1 | Arti        | Pune
  13 | aaa     | 2002-02-02 |      2 |      2 | Heena       | Pune
  14 | fff     | 2002-03-02 |      3 |      3 | Khushi      | Pune
  15 | ggg     | 2002-03-03 |      4 |      4 | Pooja       | Jiyanpur
  16 | ggghh   | 2005-05-03 |      5 |      5 | Azad        | Jiyanpur
  17 | ggkkghh | 2005-05-03 |      6 |      6 | Anand       | varansi
  10 | ccc     | 2999-09-09 |        |        |             | 
     |         |            |        |      8 | Virat singh | Jiyanpur
     |         |            |        |      7 | Azad singh  | Jiyanpur
(10 rows)
--------------------------------------------------------------------------------------------------------------

Decomposition : 

lossless : no loss of info
lossy : loss of information.


select emp_name from employee e, department d
where e.e_id = d.e_id and dept_head = 'XYZ';