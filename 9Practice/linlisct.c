#include<stdio.h>
#include<stdlib.h>

struct node{
   
   int data;
   struct node *next;
};
struct node * getNode(int value) {

    struct node *temp = malloc(sizeof(struct node *));
    temp->data = value;
    temp->next = NULL;
    return temp;
}
void initList(struct node **h) {

    *h = getNode(0);
}
void append(struct node *h,int value) {

    struct node *p = h;
    struct node *temp = getNode(value);

    while(p->next != NULL) {

        p = p->next;
    }
    p->next = temp;
    h->data++;
}
void display(struct node *h) {

    struct node *temp = h->next;

    while(temp) {

        printf("%d---->",temp->data);
        temp = temp->next;
    }
    printf("NULL\n");
}
void insert(struct node *h,int pos,int value) {

    struct node *p = h;
    struct node *q = h->next; 
    struct node *temp = getNode(value);

    int i = 0;

    while(i<pos-1) {

        p = q;
        q = q->next;
        i++;
        
    }
    p->next = temp;
    temp->next = q;
    h->data++;

}
void delete(struct node *h,int pos) {

    struct node * p = h;
    struct node * q = h->next;
    struct node *temp = NULL;

    int i = 0;

    while(i < pos-1) {

        p = q;
        q = q->next;
        i++;
    }
    temp = q;
    printf("\nDeleted element is : %d\t",temp->data);
    
    q = q->next;
    p->next = q;
    temp->next = NULL;
    free(temp);

}
int search(struct node *h, int element) {

    struct node *p = h->next;

    while(1) {

        if(p->data == element) 
            return 1;
        p = p->next;

    }
    return 0;
}
int main() {

    int ch,value;

    struct node *head = NULL;
    initList(&head);

    while(1) {

        printf("\n1: Append  2:Insert  3:Delete  4:Search  5:Display  6:Reverse linkList");
        printf("\nEnter the choice : \t");
        scanf("%d",&ch);

        switch(ch) {

            case 1 : printf("\nEnter value to append:\t");
                    scanf("%d",&ch);
                    append(head,ch);
                    break;
            case 2 : printf("Enter the pos and value for insertion :\t");
                    scanf("%d%d",&ch,&value);
                    insert(head,ch,value);
                    break; 
            case 3 : printf("Enter position to delete element : \t");
                    scanf("%d",&ch);
                    delete(head,ch);
                    break;  
            case 4 : printf("\nEnter the element : \t");
                    scanf("%d",&ch); 
                    ch = search(head,ch);
                    if(ch == 1)
                        printf("\nFOUND\n");
                    else 
                        printf("\nNOT FOUND\n");    
                    break;              
            case 5 : display(head);
                    break;

        }
    }
}