#include<stdio.h>
#include<stdlib.h>

struct node {

    int key;
    struct node *left;
    struct node *right;
};
struct node * getNode(int value) {

    struct node *temp = malloc(sizeof(struct node *));
    temp->key = value;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}
struct node * insert(struct node *root,int value) {

    if(root == NULL)
        return getNode(value);

    if(root->key < value) 
        root->right = insert(root->right,value);
    else if(root->key > value) 
        root->left = insert(root->left,value);

    return root;            
}
void preOrder(struct node *root) {

// printf("*****");
    if(root == NULL)
        return;
    printf("%d--->",root->key);    
    preOrder(root->left);
    preOrder(root->right);  
    // printf("NULL"); 
} 
void inorder(struct node *root) {

    if(root == NULL)
        return;

    inorder(root->left);
    printf("%d--->",root->key);
    inorder(root->right);
    // printf("NULL");
}
struct node * removeNode(struct node *root, int value) {

    if(root == NULL)
        return NULL;

    if(root->key < value) {

        root->right = removeNode(root->right,value);
    }    

    if(root->key > value) {

        root->left = removeNode(root->left, value);
    }

    else {

        if(root->left == NULL && root->right == NULL) {

            free(root);
            return NULL;
        }
        else if(root->right == NULL) {

            struct node *temp = root->left;
            free(root);
            return temp;
        }
        else if(root->left == NULL) {

            struct node * temp = root->right;
            free(root);
            return temp;
        }

    }
    return root;
}
int main() {

    struct node *root = NULL;
    int value;
    int ch;

    while(1) {

        printf("\n1: Insert  2: pre-order  3: In-order  4: Post-order\n");
        printf("nter the choice : \t");
        scanf("%d",&ch);

        switch(ch) {

            case 1 : printf("\nEnter the value for insertion :\t");
                    scanf("%d",&value);
                    root = insert(root,value);
                    break;

            case 2 : preOrder(root);
                    break; 

            case 3 : inorder(root);
                    break; 

            case 4: printf("\nwhich element you want to remove :\t");
                    scanf("%d",&value);
                    removeNode(root,value);
                    break;                      
        }
    }
}