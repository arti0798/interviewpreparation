
function Counter() {

    var count = 0;
    this.increment = function() {

        count++;
        console.log(count);
    }
    this.decrement = function() {

        count--;
        console.log(count);
    }
}

var cc = new Counter();

cc.increment();
cc.increment();
cc.decrement();