//When you take a function and pass it to another function is 
//known as callback funtion i.e the fun which is passed is known as call back fun.

function x(y) {

}
x(function y() {

});
// callback fun example
// -------------------------------------
//JS is a synchronous and single-threaded language i.e it will excute one line at a time
// due to call back we can do asyn things in js



// a fun which takes another fun as an argument or return a fun is 
//known as higher order function

function x() {

}
function y(x) {

    x();
}

// y is higher order fun and x is a call back fun