// 

// function a() {

//     let x = 90;
//     function b() {

//         console.log(x);
//     }
//     b();
// }

// a();

// function along with its lexical scope known as closure

// -------------------

// function is a heart of js
// 1 : assign function to a variable
// function a() {

//     let x = function b() {

//         console.log(x);
//     };
    
//     b();
// }

// a();

// tou can pass the function the as a Parameters.

// you can return the function   (return nameOfAFun)

function a() {

    let x = 90;
    function b() {

        console.log(x);
    }
    return b
}

var z = a();
console.log(z);  // return the whole b fun 
z();


// whenever the function is returned ...a. they still remember their lexical parent. => closure retrned 

// function along with its lexical bundal is known as closure


function a() {

    let x = 90;
    return function b() {

        console.log(x);
    }
    // return b         same as above
}

var z = a();
console.log(z);  // return the whole b fun 
z();

// -----------------------

function a() {

    let x = 90;
    function b() {

        console.log(x);
    }
    a = 10 
    return b     // o/p will be 10  
}

var z = a();
console.log(z);  // return the whole b fun 
z();


// use of closure

// 1: currying
// 2: Module design Pattern
// 3: setTimeouts
// 4:  Iteraters
// 5: maintain state in asyn world