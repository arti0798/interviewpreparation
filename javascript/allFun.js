// Difference in hoisting
statements(); // memory willl assign to whole funtion
b();  // Reference error because it will treate as a variable

//Function statement or decalration

function statements() {

    console.log("I m function statement");
}

//FUnction expression

var b = function () {

    console.log("I m function expression");
}

//Anonymous function

// Anonymous fun use in a way where fun use as a value

function () {

}

//Named function expression

var b = function xyz() {

    console.log(xyz);
}

var c = function ab(param1 , param2) { // parameter

}

acb(1,2,3)  //argument

//THe ability to use funtion as value and can be passed as argument to other function 
// and can be returned from a function this ability is known as first class function

