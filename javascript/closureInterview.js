// function outest() {

//     function outer(b) {

//         var a = 10;
//         function inner() {

//             console.log(a,b);
//         }
//         var a = 100;
//         return inner;
//     }
//     var a = 90;
//     return outer;
// }

// var show = outest()("ououou");
// show();
// console.log(show);


// data hiding uing closure

function cc() {

    var count = 0;
    return function counter() {

        count++;
        console.log(count);
    }
}

var show = cc();
show();
show();
show();