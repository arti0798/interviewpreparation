#include<stdio.h>

void matrixMultiply(int mat1[][10],int mat2[][10],int r,int c) {

    int i,j,k;
    int res[10][10];
    for(i = 0;i < r;i++) {

        for(j = 0;j < c;j++) {

            res[i][j] = 0;

            for(k = 0;k < r;k++) {

                res[i][j] = res[i][j] + mat1[i][k] * mat2[k][j];
            }
        }
    }

    for(i = 0;i < r;i++) {

        for(j = 0;j < c;j++) 
            printf(" %d   ",res[i][j]);
    }
    printf("\n");
}
int main() {

    int mat1[10][10],mat2[10][10];

    int r,c,j,i;

    printf("Enter the row and col : \t");
    scanf("%d%d",&r,&c);

    printf("Enter the first matrix\n");
    for(i = 0;i < r;i++) {

        for(j = 0;j < c;j++) {

            scanf("%d",&mat1[i][j]);
        }
    }

    printf("Enter the second matrix\n");
    for(i = 0;i < r;i++) {

        for(j = 0;j < c;j++) {

            scanf("%d",&mat2[i][j]);
        }
    }

    matrixMultiply(mat1,mat2,r,c);
}